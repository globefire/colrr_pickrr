<h3>Color Picker From Webcam📸🎨🔍</h3>

<img src="https://github.com/globefire/colrr_pickrr/blob/master/upload.gif" alt = "press the link below"/>

Press this <a href = "https://youtu.be/6y59fXgzMCs">link</a> to watch a longer demonstration.

Image is captured by pressing 'c' from live video, by double clicking on the image at a particular pixel you get the rgb color code.
